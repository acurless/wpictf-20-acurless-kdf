\documentclass{article}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{booktabs}
\usepackage[margin=1in]{geometry}
\begin{document}

\lstset{frame=tb,
  language=C,
  aboveskip=3mm,
  belowskip=3mm,
  xleftmargin=\parindent,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\ttfamily},
  title=\lstname,
  xleftmargin=1cm,
  breaklines=true,
  breakatwhitespace=true,
  tabsize=8
}

\newcommand*\xor{\oplus}

\title{ACurless Key Derivation Function}
\author{Adrian Curless}
\date{March 2020}
\maketitle

\section{Overview}
ACurless KDF has two distinct phases, each of which will be described in detail in this document. In the
section for each phase, the computation is described, as well as a description of any magic constants in use.

The two phases are combined with an variable length input to produce a 32 bit key, as described below.
\begin{equation*}
    \begin{aligned}
        \text{KEY} = \text{PHASE}2\ (\text{PHASE}1\ (\text{input},\ \text{iv}))
    \end{aligned}
\end{equation*}

\section{Phase One}
Phase one is based upon the well known Blowfish cipher. Phase one, like Blowfish operates using
a block size of 64 bits, which means it is well suited to implementation on modern CPUs. See
Section~\ref{bf:mods} for a description of the modifications to Blowfish.

Phase One produces $O$, which is half the length of $\text{input}$, given $\text{input}$ and an initialization
vector $\text{iv}$, which always takes the value 0xC0FFEE\@.
\begin{equation*}
    \begin{aligned}
        O = \text{PHASE}1(\text{input},\ \text{iv})
    \end{aligned}
\end{equation*}

The KDF utilizes Blowfish in a slightly unconventional way that is described in this section. See
Section~\ref{bf:sbox} for details regarding the sboxes and P-Array.


Perform the following steps to complete Phase One:
\begin{enumerate}
    \item The KDF input (a character array) must be padded so the length can be evenly divided into blocks of
        64 bits. Padding is simple, just fill the needed space with 0-bytes (0x00).
    \item Divide the input data into 64 bit blocks.
    \item Initialize the key as described in Section~\ref{bf:key}.
    \item For each block of input:
        \begin{itemize}
            \item Perform a single ACurless KDF key transformation as described in Section~\ref{kdf:cycle}.
            \item Initialize a Blowfish key using the output of the key transformation obtained above.
            \item Blowfish encrypt the current block of input.
            \item Append the upper 32 bits of the output to $O$.
            \item Use the lower 32 bits of the output as the IV for the next block.
        \end{itemize}
\end{enumerate}

To be clear, at the end of phase one will output half as many blocks as it is given. If you provided 10
blocks of input, there will be 5 blocks of output (because only half of each block is uses as output).

\subsection{P-Array and Substitution Boxes}\label{bf:sbox}
ACurless KDF uses the same sboxes as Blowfish, we can be easily obtained online. Since Blowfish has a large
sbox table, the table is omitted here for brevity. In addition to the sboxes, Blowfish makes use of a so called
`P-Array', the contents of which are also available online.

Both the sboxes and so called P-Array are composed of hexadecimal digits derived from Pi as stated in the
Blowfish specification.

\subsection{Modifications to Blowfish}\label{bf:mods}
The modifications made to the Blowfish cipher are enumerated here.

\subsubsection{Encryption}
In the encryption function, the function  $f$ is applied on both the upper and lower 32 bits of the block
of input data. When the function $f$ is applied to the upper 32 bits, a bitwise and should be computed
from the output of $f$ and the bitwise complement of the hexadecimal constant 0xFC. When the function $f$ is applied to the lower 32 bits,
a bitwise and should be computed of the output of $f$ and the bitwise and of the same constant.

In the same function, the upper 32 bits of the input block is xored with the second element of the `P-Array',
instead of the 17th as done in standard blowfish. Similarly, the lower 32 bits are xored with the first element
of the `P-Array', as opposed to the 18th.

\subsubsection{Function $f$}\label{bf:mod_f}
When looking up values in the odd numbered sboxes (1 and 3), a left circular shift of 2 is applied to the
results prior to their usage. Similarly, a right circular shift of 3 is applied to the values looked up
from the even numbered sboxes (2 and 4).

\subsubsection{Key Size}
In this modified version of Blowfish the key size is extended to 64 bytes.

\subsection{Key Initialization}\label{bf:key}
The initial key for Blowfish is a 64 byte sequence of random data. The key is shown in \
Listing~\ref{bf:ikey}.

\begin{lstlisting}[caption={Initial key used by Blowfish in ACurless KDF.}\label{bf:ikey}]
static uint8_t bf_initial_key[64] = {
    0x83, 0x25, 0x31, 0xdc, 0x3a, 0x51, 0xd5, 0x56, 0xc9, 0x09, 0x37,
    0x30, 0x97, 0xce, 0x50, 0x05, 0x5b, 0xc4, 0x1a, 0x9f, 0x74, 0x17,
    0x82, 0x35, 0x64, 0x00, 0xfe, 0xac, 0x0d, 0x88, 0x18, 0xe2, 0x42,
    0x88, 0x3c, 0x0f, 0x1a, 0xe3, 0x6a, 0x96, 0xfe, 0x73, 0xe3, 0x67,
    0x21, 0x36, 0xf5, 0x94, 0x73, 0x0d, 0x94, 0x73, 0x9d, 0x55, 0xca,
    0x7f, 0xde, 0xad, 0xbe, 0xef, 0xc0, 0xfe, 0xff, 0x01
};
\end{lstlisting}

\subsection{ACurless KDF Key Transformation}\label{kdf:cycle}
\begin{itemize}
    \item Divide the key into 32 bit chunks
    \item Replace each chunk of the key to the Exclusive Or of the chunk and the IV.
\end{itemize}

\section{Phase Two}
Phase Two accepts the output of Phase One and produces a 32 bit key (regardless of the input length).

\begin{equation*}
    \begin{aligned}
        \text{KEY} = \text{PHASE}2(O)
    \end{aligned}
\end{equation*}

Phase Two is a sponge function, designed to take the variable length $O$ produced in Phase One and make
it fixed length (32 bits). In general, as is the case in ACurless KDF, a sponge function is composed of
three components.

\begin{description}
    \item{$s$: } State containing 64 bits in ACurless KDF.
    \item{$f$: } A function that transforms $s$ using a pseudorandom permutation. See Section~\ref{sponge:lcg}.
\end{description}

The state $s$ is divided into two sections, the bitrate $r$ and the capacity $c$. For performance and
simplicity, $r$ and $c$ are both 32 bits, each one half of $s$. The lower 32 bits of $s$ are assigned
to $r$.

\subsection{Sponge Operation}

\begin{itemize}
    \item $s$ is initialized to 0.
    \item The input is divided into 32 bit blocks (The output of phase one is conveniently a multiple of 32
        bits).
    \item For each block $B$ of input,

        \begin{itemize}
            \item $r = r \xor B$
            \item $s = f(s)$
        \end{itemize}
        This is the absorption process of the `sponge'.
    \item Now in the final step the output is `squeezed' out:

        \begin{itemize}
            \item $s = f(s)$
            \item $r$ is yielded as the output.
        \end{itemize}
\end{itemize}

\subsection{Pseudorandom function $f$}\label{sponge:lcg}
The pseudorandom function $f$ is implemented as a linear congruential generator (LCG). The function $f$ takes a
64 bit input and produces a 64 bit output (i.e.\ the function is modulo $2^{64}$).

The LCG can be defined in terms of the recurrence relation:
\begin{equation*}
    \begin{aligned}
        X_{n+1} = (aX_{n} + b)\ \text{mod}\ m
    \end{aligned}
\end{equation*}

where $a = 2862933555777941757$, $b = 3037000493$, $m = 2^{64}$.

\section{Test Vectors}
The following are official test vectors for ACurless KDF.
\begin{center}
    \begin{tabular}{cc}
        Input String & Key \\
        \toprule
        Secret String & 0x56ad4102 \\
        GNU+Linux & 0x306d9005 \\
        Systemd is the worst & 0x2146b411 \\
        Install Gentoo & 0x6d707270 \\
    \end{tabular}
\end{center}

\end{document}
