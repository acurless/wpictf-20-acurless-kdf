CC = gcc
CFLAGS = -Wall -Werror -Wextra -O2 -fPIC -g
LINK =

OBJECT_DIR = obj
INCLUDE =
SRC = $(wildcard *.c)

OBJ = $(patsubst %.c,$(OBJECT_DIR)/%.o,$(SRC))

ARTIFACT = acurless-kdf

all: src

$(OBJECT_DIR)/%.o: %.c
	$(CC) -c $(INCLUDE) -o $@ $< $(CFLAGS)

$(ARTIFACT): $(OBJ)
	$(CC) -o $@ $^ $(LINK)

init:
	@mkdir -p $(OBJECT_DIR)

src: init $(ARTIFACT)

clean:
	rm -rf $(OBJECT_DIR) $(ARTIFACT)

